import React, {useRef, useEffect, useState} from 'react';
import './Network.css';
import * as d3  from 'd3';

// Import Data
import node_1 from '../../data/node_view_1.json';
import node_2 from '../../data/node_view_2.json';
import node_3 from '../../data/node_view_3.json';
import node_4 from '../../data/node_view_4.json';

import edge_1 from '../../data/edge_view_1.json';
import edge_2 from '../../data/edge_view_2.json';
import edge_3 from '../../data/edge_view_3.json';
import edge_4 from '../../data/edge_view_4.json';

// Import Components
import Popup from '../Popup/Popup';
import StaticPopup from '../StaticPopup/StaticPopup';
import DropdownMenu from '../DropdownMenu/DropdownMenu';
import TopNode from '../TopNode/TopNode';

// Data per view 
// If node is same for all view, only need edge list
const node_list = [node_1, node_2, node_3, node_4];
const edge_list = [edge_1, edge_2, edge_3, edge_4];

export default function Network( props ) {

    const data = {
        nodes : node_list[props.view],
        links : edge_list[props.view]    
    };

    const svgRef = useRef();
    const [clickedObject, setClickedObject] = useState();
    const [clickEvent, setClickEvent] = useState();

    const [dropdownObject, setDropdownObject] = useState();
    const [prevDropdownObject, setPrevDropdownObject] = useState();
    
    // Drag Functionality
    const drag = simulation => {

        function dragstarted( event, d ) {
            if (!event.active) simulation.alphaTarget(0.3).restart();
            d.fx = d.x;
            d.fy = d.y;
        }

        function dragged( event, d ) {
            d.fx = event.x;
            d.fy = event.y;
        }

        function dragended( event, d ) {
            if (!event.active) simulation.alphaTarget(0);
            d.fx = null;
            d.fy = null;
        }

        return d3.drag()
            .on("start", dragstarted)
            .on("drag", dragged)
            .on("end", dragended);
    }


    // Zoom & Pan Functionality (LATER, if needed)




    // Neighbor Helper Function 1 
    function firstNeighbor(src, tar) {
        if (tar.index == src.index) {
            return true;
        } 
        if ( (src.firstOrderNeighborIdx.includes( tar.index )) |
             (tar.firstOrderNeighborIdx.includes( src.index )) ) {
            return true;
        }

        return false;
    }

    // Neighbor Helper Function 2
    function secondNeighbor(src, tar) {
         
        if (tar.index == src.index) {
            return true;
        } 
        if ((src.secondOrderNeighborIdx.includes( tar.index )) | 
            (tar.secondOrderNeighborIdx.includes( src.index )) ){
            return true;
        }

        /* If directed,
         *
         *
         * REMOVE
         * tar.first | secondIdx includes (src) line
         *
         *
         */

        return false;
       
    }

    // Render Network Graph
    useEffect( () => {

        // On changing view, remove previous network view
        if ( d3.select(svgRef.current).select('g')._groups[0][0] != undefined ) {
            d3.select(svgRef.current)
                .selectAll('g')
                .remove();
        }

        // Create SVG container with reference element
        const svg = d3.select(svgRef.current)
            .attr('viewBox', [-300, -300, 600, 600])
            .attr('preserveAspectRatio', 'xMidYMid meet');

        // Nodes & Link -> Object
        const links = data.links.map(d => Object.create(d));
        const nodes = data.nodes.map(d => Object.create(d));

        
        // Define simulation 
        const simulation = d3.forceSimulation( nodes )
            .force("link", d3.forceLink(links).id( d=>d.id))
            .force("charge", d3.forceManyBody().strength(-60))
            .force("x", d3.forceX())
            .force("y", d3.forceY());

        simulation
            .on("tick", () => {
                link
                    .attr("x1", d=>d.source.x)
                    .attr("y1", d=>d.source.y)
                    .attr("x2", d=>d.target.x)
                    .attr("y2", d=>d.target.y);
        
                node
                    .attr("cx", d=>d.x)
                    .attr("cy", d=>d.y);

            });

        // Define Link
        const link = svg.append("g")
            .attr("stroke", "#bbb")
            .attr("stroke-opacity", d => Math.sqrt(d))
            .selectAll("line")
            .data(links)
            .join("line")
            .attr("stroke-width", d => Math.sqrt(1));


        // Define Node
        const node = svg.append("g")
            .attr('class', 'nodes')
            .attr('style', 'position : relative')
            .attr("stroke", "#abc") // variable color (highlight)
            .attr("stroke-width", 1.5)
            .selectAll("circle")
            .data(nodes)
            .join("circle")
            .attr("r", 5)
            .call( drag(simulation) );

        // Node Interaction (Mouse Hover Version)
        /*
        node
            .on('mouseover', (event,d) => {
                //
                node.style("opacity", function(o) { 
                    return firstNeighbor(d, o) ? 1 : 
                        secondNeighbor(d, o) ? 0.5 : 0.1;
                });        
            })
            .on('mouseout', (event, d) => {

                node.style("opacity", 1);
            })
        */
        // Node Interaction (Mouse Click Version)i
        var popUpToggled = false;
        var onClickedNodeIdx = -1;
        node
            .on('click', (event,d) => {
                if (popUpToggled & (event.target.tagName == 'circle')) {
                    if (onClickedNodeIdx == d.index ) {
                        node.style("opacity", 1);
                        node.style("fill", "#000000");
                        popUpToggled = false;
                        onClickedNodeIdx = -1;
                        setClickedObject(null);
                        setClickEvent(null);
                    }
                    else {
                        node.style("opacity", function(o) {
                            return firstNeighbor(d, o) ? 1 : 
                                secondNeighbor(d, o) ? 0.5 : 0.1;
                        });
                        node.style("fill", function(o) {
                            return (d == o) ? "#1c49c1" : 
                                firstNeighbor(d, o) ? "#ff1300" :
                                secondNeighbor(d, o) ? "#009E10" : "#000000";
                        });
                        onClickedNodeIdx = d.index;
                        setClickedObject(d);
                        setClickEvent(event);
                    }
                }
                else {
                    node.style("opacity", function(o) { 
                        return firstNeighbor(d, o) ? 1 : 
                            secondNeighbor(d, o) ? 0.5 : 0.1;
                    }); 
                    node.style("fill", function(o) {
                        return (d == o) ? "#1c49c1" : 
                            firstNeighbor(d, o) ? "#ff1300" :
                            secondNeighbor(d, o) ? "#009E10" : "#000000";

                    });
                    popUpToggled = true;
                    onClickedNodeIdx = d.index;
                    setClickedObject(d);
                    setClickEvent(event);
                }

            });

           

        // Edge Interaction (Mouse Hover Version)
        /*
        link
            .on("mouseover", (event, d) => {
            
                node.style("opacity", function(o) {
                            
                    return ( (d.source.index == o.index) | 
                             (d.target.index == o.index) ) ? 1 : 0.1; 
                       
                });

            })
            .on("mouseout", (event, d) => {

                node.style("opacity", 1);
            });
        */

        // Edge Interaction (Mouse Click Version)
        var edgePopUpToggled = false;
        var onClickedEdgeIdx = -1;

        link
            .on("click", (event, d) => {
                if (edgePopUpToggled & (event.target.tagName == 'line')) {
                    
                    if (onClickedEdgeIdx == d.index) {
                        node.style("opacity", 1);
                        node.style("fill", "#000000");
                        edgePopUpToggled = false;
                        onClickedEdgeIdx = -1;
                        setClickedObject(null);
                        setClickEvent(null);
                    }
                    else {
                        node.style("opacity", function(o) {
                            return ( (d.source.index == o.index) | 
                                     (d.target.index == o.index) ) ? 1 : 0.1; 
                       
                        });
                        node.style("fill", function(o) {
                            return ( (d.source.index == o.index) | 
                                     (d.target.index == o.index) ) ? 
                                     "#ff1300" : "#000000"; 
                        });
                        onClickedEdgeIdx = d.index;
                        setClickedObject(d);
                        setClickEvent(event);
                    }
                }
                else {
                    node.style("opacity", function(o) {
                        return ( (d.source.index == o.index) |
                                 (d.target.index == o.index) ) ? 1 : 0.1;
                    });
                    node.style("fill", function(o) {
                        return ( (d.source.index == o.index) | 
                                 (d.target.index == o.index) ) ? 
                                 "#ff1300" : "#000000"; 
                    });
                    edgePopUpToggled = true;
                    onClickedEdgeIdx = d.index;
                    setClickedObject(d);
                    setClickEvent(event);
                }
            });


        svg
            .on('click', (event, d) => {
                
                if( event.target.tagName != 'circle' 
                    & event.target.tagName != 'line') {

                    if (popUpToggled | edgePopUpToggled ) {
                        node.style("opacity", 1);
                        node.style("fill", "#000000");
                        popUpToggled = false;
                        edgePopUpToggled = false;
                        onClickedNodeIdx = -1;
                        onClickedEdgeIdx = -1;
                        setClickedObject(null);
                        setClickEvent(null);
                    }

                }


            });

        // Node Label with Text
        node.append("title")
            .text( d => d.label )

    }, [props]);


    // When the user select firms from dropdown menu
    useEffect(() => {

        const svg = d3.select(svgRef.current)
        const node = svg
            .selectAll('circle');

        if (dropdownObject != undefined ) {
            if (prevDropdownObject != undefined ) {
                if ( (dropdownObject.id != prevDropdownObject.id) ) { 
                    
                    // Highlight neighbors with selected object
                    node
                        .filter( function(d) {
                            return d.index == dropdownObject.id;
                        })
                        .attr("stroke", "#abc")
                        .attr("r", 10);

                    // Reset previously selected object
                    node
                        .filter( function(d) {
                            return d.index == prevDropdownObject.id;
                        })
                        .attr("stroke", "#abc")
                        .attr("r", 5);

                    setPrevDropdownObject( dropdownObject);
                }
                
            }

            else {
                
                node
                    .filter( function(d) {
                        console.log( d );
                        return d.index == dropdownObject.id;
                    })
                    .attr("stroke", "#abc")
                    .attr("r", 10);

                setPrevDropdownObject( dropdownObject);
 
            }

        }
    }, [props, dropdownObject]);


    

    return (
        <div className='networkContainer'>
            <TopNode />
            
            <svg ref={svgRef} className='network'></svg>
            {/* <Popup clickedObject={clickedObject}
                clickEvent={clickEvent}/> */}
            {/* <div className='popup_container'
                style={{
                    position: 'absolute',
                    right: '10vw',
                    bottom: '10vh',
                    width: '100px',
                    height: '100px'
                }}> */}

            {/* </div> */}
            <DropdownMenu toPassParent={setDropdownObject}
                            nodeList={node_list[props.view]} />

            <StaticPopup clickedObject={clickedObject}
                            clickEvent={clickEvent}/>

        </div>        
    );

}


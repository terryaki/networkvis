import React, {useState, useEffect, useLayoutEffect} from 'react';

function Popup(props) {

    const [popUpToggle, setPopUpToggle] = useState();
    const [displayOpt, setDisplayOpt] = useState();
    const [xCoord, setXCoord] = useState();
    const [yCoord, setYCoord] = useState();
    const [nodeLabel, setNodeLabel] = useState();
    const [edgeSource, setEdgeSource] = useState();
    const [edgeTarget, setEdgeTarget] = useState();


    useEffect(() => {

        setPopUpToggle( !(props.clickedObject == undefined | props.clickedObject == null) );
        popUpToggle ? setDisplayOpt('block') : setDisplayOpt('none');        
    
    }, [props]);

    useEffect(() => {

        if( popUpToggle & props.clickedObject != null ) {
            setXCoord( props.clickEvent.x + 10 );
            setYCoord( props.clickEvent.y + 10 );

            setNodeLabel( props.clickedObject.label );

            // setEdgeSource( props.clickedObject.source.label );
            // setEdgeTarget( props.clickedObject.target.label );
        }

    }, [props, popUpToggle]);
    


    return (
        <div className='textBox' 
            style={{
                'backgroundColor' : 'red',
                'display' : displayOpt,
                'position' : 'absolute',
                'left' : xCoord + 'px',
                'top' : yCoord + 'px',
            }}>
            <h3>
                {nodeLabel}
                {edgeSource}
                {edgeTarget}
            </h3>
        </div>
    );

}


export default Popup;

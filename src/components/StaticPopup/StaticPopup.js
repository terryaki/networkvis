import React, {useState, useEffect} from 'react';

function StaticPopup(props) {

    const [popUpToggle, setPopUpToggle] = useState();
    const [displayOpt, setDisplayOpt] = useState();

    const xCoord = '100px';
    const yCoord = '100px';

    const [nodeLabel, setNodeLabel] = useState();

    useEffect(() => {

        setPopUpToggle( !(props.clickedObject == undefined | props.clickedObject == null) );
        popUpToggle ? setDisplayOpt('block') : setDisplayOpt('none');        
    
    }, [props]);

    useEffect(() => {

        if( popUpToggle & props.clickedObject != null) {
            setNodeLabel( props.clickedObject.label);
        }
        else{
            setNodeLabel("");
        }

    }, [props, popUpToggle]);
    

    return (
        <div className='textBox' 
            style={{
                'backgroundColor' : 'green',
                'display' : displayOpt,
                'position' : 'absolute',
                'right' : '2.5vw',
                'bottom' : '50px',
                'width' : '20vw',
                'height' : '150px'
            }}>
            <h3>
                {nodeLabel}
            </h3>
        </div>
    );

}


export default StaticPopup;

import React, {useRef, useState, useEffect} from 'react';
import './DropdownMenu.css';

function DropdownMenu(props) {


    const dropdownRef = useRef(null); 

    const firms = props.nodeList;

    var searchResult = [];
    const [searchKey, setSearchKey] = useState("");
    const [showResult, setShowResult] = useState("none");
    const [btnList, setBtnList] = useState();
    const [click, setClick] = useState(false);

    // Update Search Result as user types in    
    const onFirmSearch = (e) => {
        setClick(true);
        setSearchKey( e.target.value );
    }

    // Show Result Box when user click the input area
    // and show the result if keyword already typed in
    const onSearchStart = (e) => {
        setClick(true);
        setShowResult('flex');
        setSearchKey( e.target.value );
    }


    // Pass the selected object to parent component 
    // (Network)
    const passToParentHandler = (obj) => {

        setShowResult('none');
        props.toPassParent( obj );

    }

    // Click Handler for click outside the input area
    // to close the result box if user click outside
    const handleClick = e => {

        if (dropdownRef.current.contains( e.target )) {
            setClick(true);
            //console.log("click inside");
        }
        else {
            setClick(false);
            setShowResult('none');
        }
    }

    // Event listener function for clicking 
    useEffect(() => {
        document.addEventListener("mousedown", handleClick);

        return () => {
            document.removeEventListener("mousedown", handleClick);
        }

    }, []);

    // render search result 
    // search result : the keyword == the beginning of target strings
    // if indexOf > 0 : the keyword "in" target strings
    useEffect(() => {

        if (searchKey == '') {
            firms.map( function(obj) {
                searchResult.push(obj);
            });
        }
        else {
            
            firms.map( function(obj) {
                if( (obj.label.toLowerCase()).indexOf(searchKey) == 0) {
                    searchResult.push(obj);
                }
            });

        }


        setBtnList( searchResult.map( (obj) => (
            <button className='searchResultBtn'
                onClick={ () => {passToParentHandler(obj)}}> 
                {obj.label}   
            </button>
        )));


    }, [click, searchKey]);


    

    return (
    
        <div ref={dropdownRef} 
            className='searchArea'>
            <input className='firmSearch' 
                placeholder='Select Firm'
                name='searchKey'
                onChange={onFirmSearch}
                onClick={onSearchStart}/>
                        
            <div className='searchResultContainer'
                style={{
                    'display': showResult,
                    'flexDirection' : 'column',
                    'overflowY' : 'scroll',
                    'height': '15vh',
                    'marginLeft' : '5px',
                    'marginRight' : '5px'
                }}>
                
                {
                    btnList
                }
                
            </div>
        </div>
    



    );

}


export default DropdownMenu;

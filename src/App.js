import './App.css';
import react, {useRef, useEffect, useState} from 'react';

import Network from './components/Network/Network';


function App() {
    
    const containerRef = useRef(null);
    const [view, setView] = useState(0);
    
    useEffect(()=>{
        console.log(containerRef.current ? containerRef.current.offsetWidth : 0);
    }, [containerRef]);

    const handleBtnClick = (e) => {
        setView( e.target.id[4]-1);
    }

    return (
        <div className='container' ref={containerRef}>
            <div className='header'>
                <h1>
                    SGER Network Visualization
                </h1>
            </div>

            <div className='uiContainer'>

                <div className='buttonContainer'>
                    <button className='viewButton' id='opt_1' onClick={handleBtnClick}>
                        View 1
                    </button>
                    <button className='viewButton' id='opt_2' onClick={handleBtnClick}>
                        View 2
                    </button>
                    <button className='viewButton' id='opt_3' onClick={handleBtnClick}>
                        View 3
                    </button>
                    <button className='viewButton' id='opt_4' onClick={handleBtnClick}>
                        View 4
                    </button>
                </div>
                <div className='networkUIContainer'>    
                    <Network view={view}/>
                </div>
            </div>

            <div className='footer'>
                <h3>
                    SNU Data Mining Lab
                </h3>
            </div>

        </div>
    );
}

export default App;
